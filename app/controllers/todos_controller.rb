class TodosController < ApplicationController

	def new 

		@todo = Todo.new

	end

	def create 

		@todo = Todo.new(todo_params)
		if @todo.save
            flash[:notice]="Se creo con exito"
		    redirect_to todo_path(@todo)
		else 
		   render 'new'
		end   

	end 

	def show
        @todo = Todo.find(params[:id])
	end 

	def edit
		@todo = Todo.find(params[:id])
	end	

	def update 
        @todo = Todo.find(params[:id])
        if @todo.update(todo_params)
        	flash[:notice] = "Se actualizo con exito"
        	redirect_to todo_path(@todo)
        else
            render 'editar'
        end	
	end	

	private 

	   def todo_params

	   	  params.require(:todo).permit(:name, :phone)

	   end 	

end 